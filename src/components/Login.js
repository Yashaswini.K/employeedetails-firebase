import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import LoginContext from "../context/loginContext";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

export default function Login() {
  const context = useContext(LoginContext);
  const history = useHistory();
  const [username1, setUsername1] = useState("");

  const [password1, setPassword1] = useState("");

  const login = (event) => {
    const data = localStorage.getItem("data");

    console.log(data);

    let reqobj = JSON.parse(data);
    console.log("reqobj", reqobj);
    console.log(reqobj.username);
    console.log(username1);

    if (reqobj.username === username1 && reqobj.password === password1) {
      toast.success("User Login is completed", {
        autoClose: 2000,
        position: toast.POSITION.BOTTOM_CENTER,
      });
      // alert("User Login is completed");
      context.changeLogin(true);

      history.push("/Register");
    } else {
      // alert("Invalid username and password");
      toast.error("Invalid username and password", {
        autoClose: 2000,
        position: toast.POSITION.BOTTOM_CENTER,
      });
      
    }
    event.preventDefault();
  };
  const register = () => {
    history.push("/Reg1");
  };

  return (
    <form onSubmit={login}>
      <div>
        <div className="col-md-4 offset-md-4 mt-5">
          <div className="card">
            <div className="card-header text-center text-black"style={{backgroundColor:"#c4c3bc"}}>
              <h3>Sign In to Account</h3>
            </div>
            <div className="card-body">
              <div className="form-group">
                <label for="email">Username *</label>
                <input
                  id="email"
                  type="text"
                  name="username1"
                  value={username1}
                  required
                  onChange={(e) => setUsername1(e.target.value)}
                  className="form-control"
                />
              </div>
              <div className="form-group">
                <label for="password">Password *</label>
                <input
                  type="password"
                  id="password"
                  type="text"
                  value={password1}
                  required
                  onChange={(e) => setPassword1(e.target.value)}
                  className="form-control"
                  required
                />
              </div>
            </div>
            <div className="card-footer">
              <button
                className="btn btn-dark float-right" 
                onClick={login}
                style={{ marginLeft: "650px" }}
              >
                SignIn
              </button>
              <button
                className="btn btn-dark "
                onClick={register}
                style={{ marginLeft: "0px" ,marginTop:"-60px"}}
              >
                SignUp
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}
