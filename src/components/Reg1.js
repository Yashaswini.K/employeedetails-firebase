import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();


export default function Register() {
  let history = useHistory();
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
 
  const submit = (event) => {
    const data = {
      username: `${username}`,
      email: `${email}`,
      password: `${password}`,
    };
    localStorage.setItem("data", JSON.stringify(data));
    if (data.password !=='' && data.username!=='') {

      // alert(`Registration Successfull`);
      toast.success("Record Added successfully", {
        autoClose: 2000,
        position: toast.POSITION.BOTTOM_CENTER,
      });
      history.push("/Login");
     
    } else {
      toast.error("Enter valid data", {
        autoClose: 2000,
        position: toast.POSITION.BOTTOM_CENTER,
      });
      // alert("Enter valid data");
    }

  };
  const login = () => {
    history.push("/Login");
  };
  return (
    <form >
      <div>
        <div className="col-md-4 offset-md-4 mt-5">
          <div className="card">
            <div className="card-header text-center text-black"style={{backgroundColor:"#c4c3bc"}}>
              <h3>Sign Up to Account</h3>
            </div>
            <div className="card-body">
              <div className="form-group">
                <label for="email">Usename *</label>
                <input
                  type="text"
                  name="username"
                  
                  value={username}
                  id="name"
                  className="form-control"
                  required
                  onChange={(e) => setUsername(e.target.value)}

                />
              </div>
              <div className="form-group">
                <label for="password">Email ID *</label>
                <input
                  type="email"
                  id="email"
                  name="email"
                  value={email}
                  required
                  className="form-control"
                  required
                  onChange={(e) => setEmail(e.target.value)}

                />
              </div>
              <div className="form-group">
                <label for="password">Password *</label>
                <input
                  type="password"
                  id="password"
                  type="text"
                  name="password"
                  value={password}
                  required
                  className="form-control"
                  required
                  onChange={(e) => setPassword(e.target.value)}

                />
              </div>
            </div>
            <div className="card-footer">
              <button
                className="btn btn-dark float-right"
                onClick={submit}
                style={{ marginLeft: "650px" }}
              >
                Sign Up
              </button>

              <button
                className="btn btn-dark "
                onClick={login}
                style={{ marginLeft: "0px", marginTop: "-60px" }}
              >
                Back
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}
